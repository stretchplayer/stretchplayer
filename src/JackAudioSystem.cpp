/*
 * Copyright(c) 2009 by Gabriel M. Beddingfield <gabriel@teuton.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

#include "JackAudioSystem.hpp"
#include "Configuration.hpp"
#include <cassert>
#include <cstring>
#include <cstdlib>
#include <jack/jack.h>
#include <jack/session.h>
#include <QString>

#include "Engine.hpp" // For JackSession
#include <cstdio>     // For JackSession
#include <QFileInfo>  // For JackSession
#include "PlayerWidget.hpp" // For JackSession

namespace StretchPlayer
{
    extern const char* command_line_full_path;

    JackAudioSystem::JackAudioSystem() :
	_client(0),
	_config(0)
    {
	_port[0] = 0;
	_port[1] = 0;
    }

    JackAudioSystem::~JackAudioSystem()
    {
	cleanup();
    }

    int JackAudioSystem::init(QString * app_name, Configuration *config, QString *err_msg)
    {
	QString name("StretchPlayer"), err;

	if(config == 0) {
	    err = "The JackAudioSystem::init() function must have a non-null config parameter.";
	    goto init_bail;
	}
	_config = config;

	if(app_name) {
	    name = *app_name;
	}

	if(config->jack_session_id().isEmpty()) {
	    _client = jack_client_open(name.toAscii(), JackNullOption, 0);
	} else {
	    _client = jack_client_open(name.toAscii(), JackSessionID, 0, config->jack_session_id().toAscii().data());
	}

	if(!_client) {
	    err = "Could not set up JACK";
	    goto init_bail;
	}

	_port[0] = jack_port_register( _client,
					"left",
					JACK_DEFAULT_AUDIO_TYPE,
					JackPortIsOutput,
					0 );
	if(!_port[0]) {
	    err = "Could not set up left out";
	    goto init_bail;
	}

	_port[1] = jack_port_register( _client,
					"right",
					JACK_DEFAULT_AUDIO_TYPE,
					JackPortIsOutput,
					0 );
	if(!_port[1]) {
	    err = "Could not set up right out";
	    goto init_bail;
	}

	if(jack_set_session_callback) {
	    jack_set_session_callback( _client,
				       JackAudioSystem::static_session_callback,
				       this );
	}

	return 0;

    init_bail:
	if(err_msg) {
	    *err_msg = err;
	}
	return 0xDEADBEEF;
    }

    void JackAudioSystem::cleanup()
    {
	deactivate();
	if( _port[0] ) {
	    assert(_client);
	    jack_port_unregister(_client, _port[0]);
	    _port[0] = 0;
	}
	if( _port[1] ) {
	    assert(_client);
	    jack_port_unregister(_client, _port[1]);
	    _port[1] = 0;
	}
	if(_client) {
	    jack_client_close(_client);
	    _client = 0;
	}
    }

    int JackAudioSystem::set_process_callback(process_callback_t cb, void* arg, QString* err_msg)
    {
	assert(_client);

	int rv = jack_set_process_callback( _client,
					    cb,
					    arg );
	if(rv && err_msg) {
	    *err_msg = "Could not set up jack callback.";
	}
	return rv;
    }

    int JackAudioSystem::set_segment_size_callback(segment_size_callback_t cb, void* arg, QString* err_msg)
    {
	assert(_client);

	int rv = jack_set_buffer_size_callback( _client,
						cb,
						arg );
	if(rv && err_msg) {
	    *err_msg = "Could not set up jack callback.";
	}
	return rv;
    }

    int JackAudioSystem::activate(QString *err_msg)
    {
	assert(_client);
	assert(_port[0]);
	assert(_port[1]);
	int rv = 0;

	jack_activate(_client);

	if( _config->autoconnect() ) {
	    // Autoconnection to first two ports we find.
	    const char** ports = jack_get_ports( _client,
						 0,
						 JACK_DEFAULT_AUDIO_TYPE,
						 JackPortIsInput
		);
	    int k;
	    for( k=0 ; ports && ports[k] != 0 ; ++k ) {
		if(k==0) {
		    rv = jack_connect( _client,
				       jack_port_name(_port[0]),
				       ports[k] );
		} else if (k==1) {
		    rv = jack_connect( _client,
				       jack_port_name(_port[1]),
				       ports[k] );
		} else {
		    break;
		}
		if( rv && err_msg ) {
		    *err_msg = "Could not connect output ports";
                    rv = AudioSystem::ENOAUTOCONNECT;
		}
	    }
	    if(k==0 && err_msg) {
		*err_msg = "There were no output ports to connect to.";
		rv = AudioSystem::ENOAUTOCONNECT;
	    }
	    if(ports) {
		free(ports);
	    }
	}
	return rv;
    }

    int JackAudioSystem::deactivate(QString *err_msg)
    {
	int rv = 0;
	if(_client) {
	    rv = jack_deactivate(_client);
	}
	return rv;
    }

    AudioSystem::sample_t* JackAudioSystem::output_buffer(int index)
    {
	jack_nframes_t nframes = output_buffer_size(index);

	if(index == 0) {
	    assert(_port[0]);
	    return static_cast<float*>( jack_port_get_buffer(_port[0], nframes) );
	}else if(index == 1) {
	    assert(_port[1]);
	    return static_cast<float*>( jack_port_get_buffer(_port[1], nframes) );
	}
	return 0;
    }

    uint32_t JackAudioSystem::output_buffer_size(int /*index*/)
    {
	if( !_client ) return 0;
	return jack_get_buffer_size(_client);
    }

    uint32_t JackAudioSystem::sample_rate()
    {
	if( !_client ) return 0;
	return jack_get_sample_rate(_client);
    }

    float JackAudioSystem::dsp_load()
    {
	if( !_client )  return -1;
	return jack_cpu_load(_client)/100.0;
    }

    uint32_t JackAudioSystem::time_stamp()
    {
	if( !_client ) return 0;
	return jack_frame_time(_client);
    }

    uint32_t JackAudioSystem::segment_start_time_stamp()
    {
	if( !_client ) return 0;
	return jack_last_frame_time(_client);
    }

    uint32_t JackAudioSystem::current_segment_size()
    {
	if( !_client ) return 0;
	return jack_get_buffer_size(_client);
    }

    void JackAudioSystem::session_callback(jack_session_event_t *event)
    {
	const int BUFSIZE = 1024 * 2;
	char cli[BUFSIZE] = "";
	Engine *e = _config->engine();
	assert(e);

	float position = e->get_position();
	float loop_a = e->get_loop_a();
	float loop_b = e->get_loop_b();
	float speed = e->get_stretch() * 100.0;
	int pitch = e->get_pitch();
	char pitch_sign[] = "-";
	QFileInfo current_song( _config->current_song() );
	QString fn;

	if(position < 0.0)
	    position = 0.0;
	if(loop_a < 0.0)
	    loop_a = 0.0;
	if(loop_b < 0.0)
	    loop_b = 0.0;

	if(pitch<0) {
	    pitch = -pitch;
	} else {
	    pitch_sign[0] = 0;
	}

	/* Always do a symlink -- no deep copies.
	 *
	 * This is the work-around for the fact that file copy
	 * operations will time-out with the JACK server.  To
	 * handle this properly means that it needs to be pushed
	 * to another thread.
	 */
	bool deep_copy = true;
#if 0
	switch (event->type) {
	case JackSessionSave:
	case JackSessionSaveAndQuit:
	    deep_copy = true;
	    break;
	case JackSessionSaveTemplate:
	    deep_copy = false;
	    break;
	}
#else
	deep_copy = false;
#endif

	int symlink_redirect_tries = 5;
	while( symlink_redirect_tries--
	       && current_song.exists()
	       && current_song.isSymLink() ) {
	    current_song = QFileInfo( current_song.symLinkTarget() );
	}

	QFileInfo target_song( QString("%1%2")
			       .arg(event->session_dir)
			       .arg(current_song.fileName()) );
	target_song.setCaching(false);

	if( target_song.exists() ) {
	    if(target_song == current_song) {
		if( target_song.isSymLink() && deep_copy ) {
		    current_song = QFileInfo( current_song.symLinkTarget() );
		    QFile::remove( target_song.absoluteFilePath() );
		}
	    } else {
		QFile::remove( target_song.absoluteFilePath() );
	    }
	}

	if( ! target_song.exists() ) {
	    if( deep_copy ) {
		QFile::copy( current_song.absoluteFilePath(),
			     target_song.absoluteFilePath() );
	    } else {
		QFile::link( current_song.absoluteFilePath(),
			     target_song.absoluteFilePath() );
	    }
	}

	if( target_song.exists() && target_song.isFile() ) {
	    fn = target_song.fileName();
	    snprintf(cli,
		     BUFSIZE,
		     "stretchplayer -J -x -s %s -^ %g -a %g -b %g -%% %g -~ %d%s \"${SESSION_DIR}%s\"",
		     event->client_uuid,
		     position,
		     loop_a,
		     loop_b,
		     speed,
		     pitch,
		     pitch_sign,
		     fn.toLocal8Bit().data());
	} else {
	    snprintf(cli,
		     BUFSIZE,
		     "stretchplayer -J -x -s %s",
		     event->client_uuid);
	}
	event->command_line = strdup(cli);

	event->flags = (JackSessionFlags)0;
	event->future = 0;

	jack_session_reply(_client, event);
	if(event->type == JackSessionSaveAndQuit)
	    _config->top_widget()->close();

	jack_session_event_free(event);
    }

} // namespace StretchPlayer
