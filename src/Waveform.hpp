/*
 * Copyright(c) 2009 by Gabriel M. Beddingfield <gabriel@teuton.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
#ifndef WAVEFORM_HPP
#define WAVEFORM_HPP

#include <QWidget>
#include <QPoint>
#include <vector>
#include <memory>

class QPaintEvent;
class QImage;

namespace StretchPlayer
{

class PlayerSizes;
class Engine;

namespace Widgets
{
    class Waveform : public QWidget
    {
    public:
	Waveform(QWidget *parent, PlayerSizes *sizes);
	~Waveform();

	void set_audio( const float *left,
			const float *right,
			unsigned long count,
			unsigned long loop_a_offset,
			unsigned long loop_b_offset,
                        unsigned long clip_offset);

	void cancel_audio();
        void set_engine(Engine *e) {
            _engine = e;
        }

    private:
	virtual void paintEvent(QPaintEvent *ev);
        virtual void resizeEvent(QResizeEvent *ev);
        virtual void mousePressEvent(QMouseEvent *ev);
        virtual void mouseReleaseEvent(QMouseEvent *ev);
        virtual void mouseMoveEvent(QMouseEvent *ev);
        void _update_palette();
        void _regenerate_waveform_image();
        void _reset_image_size(const QSize& w_size);
        void _change_ab_loop(unsigned long new_a, unsigned long new_b);

    private:
	bool _valid;
	const float *_left, *_right;
	unsigned long _count, _loop_a, _loop_b, _clip_offset;
	std::vector<float> _buf;
        PlayerSizes *_sizes;
        std::auto_ptr<QImage> _image;
        Engine *_engine;

        struct MouseTracking_t {
            bool active;
            bool ed_loop_a;
            QPoint start;
            QPoint latest;
            int radius; // How close to loop boundary to lock?
        } _mouse;
    };

} // namespace Widgets

} // namespace StretchPlayer

#endif // WAVEFORM_HPP
