/*
 * Copyright(c) 2009 by Gabriel M. Beddingfield <gabriel@teuton.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

#include "Waveform.hpp"
#include "PlayerSizes.hpp"
#include "Engine.hpp"

#include <QPainter>
#include <QPen>
#include <QMouseEvent>
#include <QImage>

#include <cmath>
#include <cassert>

#include <QDebug>
#include <iostream>
using namespace std;

namespace StretchPlayer
{

namespace Widgets
{

    Waveform::Waveform(QWidget *parent, PlayerSizes *sizes) :
	QWidget(parent),
	_valid(false),
	_left(0),
	_right(0),
	_count(0),
	_loop_a(0),
	_loop_b(0),
        _sizes(sizes),
        _image(0),
        _engine(0)
    {
        _image.reset( new QImage( size(), QImage::Format_ARGB32 ) );
        _mouse.active = false;
        _mouse.ed_loop_a = false;
        _mouse.radius = 12;
    }

    Waveform::~Waveform()
    {
    }

    void Waveform::set_audio( const float *left,
                              const float *right,
                              unsigned long count,
                              unsigned long loop_a_offset,
                              unsigned long loop_b_offset,
                              unsigned long clip_offset )
    {
        if( count != _count
            && clip_offset != _clip_offset
            && loop_a_offset != _loop_a
            && loop_b_offset != _loop_b
            && left != _left
            && right != _right ) {
            _valid = false;
            _buf.clear();
            _left = left;
            _right = right;
            _count = count;
            _loop_a = loop_a_offset;
            _loop_b = loop_b_offset;
            _clip_offset = clip_offset;

            if(_left && _right && _count)
                _valid = true;

            _reset_image_size( size() );
            _regenerate_waveform_image();
        }

    }

    void Waveform::_regenerate_waveform_image()
    {
        assert( _image.get() );
        QPainter p(_image.get());
        QBrush brush( palette().color(QPalette::Active, QPalette::Window ) );
        QPen pen( palette().color(QPalette::Active, QPalette::Dark) );
        int w, h;
        int img_w, img_h;
        int wid_w, wid_h;

        img_w = _image->width();
        img_h = _image->height();
        wid_w = width();
        wid_h = height();

        p.setRenderHints(QPainter::Antialiasing);

        p.drawRect( _image->rect() );

        pen.setColor( palette().color(QPalette::Active, QPalette::Mid ) );
        pen.setWidthF( 2.0 );
        p.setPen(pen);
        p.drawLine( 0, img_h/2, img_w, img_h/2 );

        if( ! (_left && _right) )
            return;

        size_t k;
        float y_scale, y_off, x_scale;
        y_scale = img_h / 4.0;
        y_off = img_h / 2.0;
        x_scale = double(img_w) / double(_count);

        pen.setColor( palette().color(QPalette::Active, QPalette::WindowText ) );
        pen.setWidthF( 1.0 );
        p.setPen(pen);
        QPointF pt_a, pt_b;
        float bin_min, bin_max, samp = 0.0f;
        int bin=0, b;
        k = 0;
        while(bin < wid_w) {
            if(k >= _count) break;
            b = wid_w * float(k) / float(_count) + 0.5;
            if( b != bin ) {
                pt_a.setX(bin);
                pt_b.setX(bin);
                pt_a.setY( bin_max * y_scale + y_off );
                pt_b.setY( bin_min * y_scale + y_off );
                p.drawLine( pt_a, pt_b );
                bin_max = bin_min = samp;
                bin = b;
            }
            samp = _left[k] + _right[k];
            if(samp > bin_max) bin_max = samp;
            if(samp < bin_min) bin_min = samp;
            ++k;
        }
    }

    void Waveform::_reset_image_size(const QSize& widget_size)
    {
        QSize isz( widget_size.width() - 4, widget_size.height() * .875 );
        _image.reset( new QImage( isz, QImage::Format_RGB32 ) );
        _image->fill(0);
    }

    void Waveform::resizeEvent(QResizeEvent *ev)
    {
        _reset_image_size( ev->size() );
        _regenerate_waveform_image();
    }

    void Waveform::cancel_audio()
    {
	_valid = false;
	_left = _right = 0;
	_count = _loop_a = _loop_b = 0;
	_buf.clear();
    }

    void Waveform::mousePressEvent(QMouseEvent *ev)
    {
        if( ev->buttons() & Qt::LeftButton ) {
            float x_a, x_b;
            int dx_a, dx_b;
            x_a = float(width()) * float(_loop_a) / float(_count);
            x_b = float(width()) * float(_loop_b) / float(_count);
            dx_a = ev->pos().x() - x_a;
            if( dx_a < 0 ) dx_a = -dx_a;
            dx_b = ev->pos().x() - x_b;
            if( dx_b < 0 ) dx_b = -dx_b;
            if(dx_a <= _mouse.radius) {
                _mouse.active = true;
                _mouse.ed_loop_a = true;
                _mouse.latest = ev->pos();
                _mouse.start = ev->pos();
            } else if (dx_b <= _mouse.radius) {
                _mouse.active = true;
                _mouse.ed_loop_a = false;
                _mouse.latest = ev->pos();
                _mouse.start = ev->pos();
            }
        }
    }

    void Waveform::mouseReleaseEvent(QMouseEvent *ev)
    {
        if( _mouse.active && (ev->button() & Qt::LeftButton) ) {
            int dx = ev->x() - _mouse.start.x();
            long frames = float(dx) * float(_count) / width() + 0.5f;

            unsigned long new_a, new_b;
            new_a = _loop_a + _clip_offset;
            new_b = _loop_b + _clip_offset;
            if(_mouse.ed_loop_a) {
                if( frames > 0 ) {
                    new_a += (unsigned long)frames;
                } else {
                    frames = -frames;
                    if( frames > new_a )
                        new_a = 1;
                    else
                        new_a -= (unsigned long)(frames);
                }
            } else {
                if( frames > 0 ) {
                    new_b += (unsigned long)frames;
                } else {
                    frames = -frames;
                    if( frames > new_b )
                        new_b = 0;
                    else
                        new_b -= (unsigned long)(frames);
                }
            }

            _change_ab_loop(new_a, new_b);

            // Clean it up.
            _mouse.active = false;
            _mouse.start = QPoint();
        }
    }

    void Waveform::_change_ab_loop(unsigned long new_a, unsigned long new_b)
    {
        if(_engine) {
            _engine->set_ab_loop(new_a, new_b);
        }
    }

    void Waveform::mouseMoveEvent(QMouseEvent *ev)
    {
        if( _mouse.active && (ev->buttons() & Qt::LeftButton) ) {
            _mouse.latest = ev->pos();
        }
    }

    void Waveform::paintEvent(QPaintEvent * /*ev*/)
    {
	QPainter p(this);

        p.setRenderHints(QPainter::Antialiasing);
        p.setBackgroundMode(Qt::TransparentMode);

        int radius = _sizes->thicker_line() * 2.0;

	p.setBrush( QColor( Qt::black ) );
        p.setPen( QColor( Qt::black ) );
	p.drawRoundedRect( rect(), radius, radius );

        // Solve all of the geometry.
        QRectF w_rec = rect();
        QRectF i_rec = _image->rect();
        QRectF left_shadow, right_shadow;
        QLineF play_head;
        unsigned long pos;
        if(i_rec.height() < w_rec.height()) {
            i_rec.translate( 0, (w_rec.height() - _image->height()) / 2 );
        }
        if(i_rec.width() < w_rec.width()) {
            i_rec.translate( (w_rec.width() - _image->width()) / 2, 0 );
        }
        float a = i_rec.width() * float(_loop_a) / float(_count);
        float b = i_rec.width() * float(_loop_b) / float(_count);
        if(_mouse.active) {
            if(_mouse.ed_loop_a) {
                a = _mouse.latest.x();
            } else {
                b = _mouse.latest.x();
            }
        }
        left_shadow = i_rec;
        left_shadow.setWidth( a );
        right_shadow = i_rec;
        right_shadow.setWidth( i_rec.width() - b );
        right_shadow.translate( b, 0 );
        pos = 0;
        if(_engine) {
            pos = _engine->get_position_frame();
            if(pos > _clip_offset)
                pos -= _clip_offset;
            else
                pos = 0;
        }
        pos = double(_image->width()) * pos / double(_count) + 0.5;
        play_head.setLine( 0, 0, 0, i_rec.height() );
        play_head.translate( i_rec.topLeft() );
        play_head.translate( pos, 0 );


        // Draw image
        p.drawImage(i_rec, *_image, _image->rect(), Qt::ColorOnly);

        // Draw shadows
        QColor filter( Qt::white );
        filter.setAlpha( 127 );
        QBrush br( filter );
        QPen pen( filter );
        p.setBrush(br);
        p.setPen(pen);
        p.drawRect( left_shadow );
        p.drawRect( right_shadow );

        // Draw playhead
        filter = QColor( Qt::yellow );
        filter.setAlpha( 0xFF );
        pen.setColor( filter );
        pen.setWidth( 3.0 );
        br.setColor( filter );
        p.setPen(pen);
        p.setBrush(br);
        p.drawLine( play_head );

    }

    void Waveform::_update_palette()
    {
	QPalette p(parentWidget()->palette());

	QColor base, bright, light, mid, dark;
	base = p.color(QPalette::Active, QPalette::Dark);
	bright = p.color(QPalette::Active, QPalette::BrightText);
	light = p.color(QPalette::Active, QPalette::Light);
	mid = p.color(QPalette::Active, QPalette::Mid);
	dark = p.color(QPalette::Active, QPalette::Dark);

	p.setColorGroup( QPalette::Active,
			 bright, // Window Text
			 dark, // Button
			 light, // light
			 dark, // dark
			 mid, // mid
			 light, // text
			 light, // bright text
			 base, // base
			 dark // window
	    );
	setPalette(p);
	
    }

} // namespace Widgets

} // namespace StretchPlayer
