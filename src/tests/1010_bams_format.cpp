/*
 * Copyright(c) 2011 by Gabriel M. Beddingfield <gabriel@teuton.org>
 *
 * This file is part of StretchPlayer
 *
 * Tritium is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Tritium is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/**
 * 1010_bams_format.cpp
 *
 * Unit tests for testing the BAMS format conversion utils.
 */

#include "bams_format.h"
#include <stdint.h>

// CHANGE THIS TO MATCH YOUR FILE:
#define THIS_NAMESPACE t_bams_format
#include "test_macros.hpp"
#include "test_config.hpp"

namespace THIS_NAMESPACE
{

    struct Fixture
    {
	// SETUP AND TEARDOWN OBJECTS FOR YOUR TESTS.

	Fixture() {}
	~Fixture() {}
    };

    template <typename DEST_TYPE, typename SRC_TYPE>
    class BufferConvertTest
    {
    public:
        const unsigned bufsize;
        SRC_TYPE *src;
        DEST_TYPE *dest;
        DEST_TYPE *expected_dest;
        int src_stride;
        int dest_stride;
        void (*copy_func)(DEST_TYPE *d, int dest_stride,
                          SRC_TYPE *s, int src_stride,
                          unsigned long count);
        BufferConvertTest(unsigned buf_size) :
            bufsize(buf_size),
            copy_func(0),
            src_stride(1),
            dest_stride(1)
            {
                src = new SRC_TYPE[bufsize];
                dest = new DEST_TYPE[bufsize];
                expected_dest = new DEST_TYPE[bufsize];
                memset(src, 0, bufsize*sizeof(SRC_TYPE));
                memset(dest, 0, bufsize*sizeof(DEST_TYPE));
                memset(expected_dest, 0, bufsize*sizeof(DEST_TYPE));
            }

        ~BufferConvertTest() {
            delete src;
            delete dest;
            delete expected_dest;
        }

        void run_test() {
            CK(copy_func);
            CK(src_stride == 1); // Other strides currently unsupported
            CK(dest_stride == 1);
            copy_func(dest, dest_stride, src, src_stride, bufsize);
            unsigned k;
            for( k=0 ; k<bufsize ; ++k ) {
                CK(dest[k] == expected_dest[k]);
            }
        }
    };
} // namespace THIS_NAMESPACE

TEST_BEGIN( Fixture );

TEST_CASE( 010_defaults )
{
    // Reserved for future "default"-ey stuff
}

TEST_CASE( 020_bams_byte_reorder_in_place )
{
    const unsigned BUFSIZE = 16;
    const char c_src[BUFSIZE] = {
        0, 1, 2, 3, 4, 5, 6, 7,
        8, 9, 10, 11, 12, 13, 14, 15
    };

    {
        char src[BUFSIZE];
        unsigned k;
        memcpy(src, c_src, BUFSIZE*sizeof(char));
        bams_byte_reorder_in_place(src, 1, 1, BUFSIZE);
        for( k=0 ; k<BUFSIZE ; ++k ) {
            CK( src[k] == c_src[k] );
        }
    }
    {
        /* 2-byte reverse, stride 1 */
        char src[BUFSIZE];
        char expected[BUFSIZE] = {
            1, 0, 3, 2, 5, 4, 7, 6,
            9, 8, 11, 10, 13, 12, 15, 14
        };
        unsigned k;
        memcpy(src, c_src, BUFSIZE*sizeof(char));
        bams_byte_reorder_in_place(src, 2, 1, BUFSIZE/2);
        for( k=0 ; k<BUFSIZE ; ++k ) {
            CK( src[k] == expected[k] );
        }
    }
    {
        /* 2-byte reverse, stride 2 */
        char src[BUFSIZE];
        char expected[BUFSIZE] = {
            1, 0, 2, 3, 5, 4, 6, 7,
            9, 8, 10, 11, 13, 12, 14, 15
        };
        unsigned k;
        memcpy(src, c_src, BUFSIZE*sizeof(char));
        bams_byte_reorder_in_place(src, 2, 2, BUFSIZE/4);
        for( k=0 ; k<BUFSIZE ; ++k ) {
            CK( src[k] == expected[k] );
        }
    }
    {
        /* 3-byte reverse, stride 2 */
        char src[BUFSIZE];
        char expected[BUFSIZE] = {
            2, 1, 0, 3, 4, 5,
            8, 7, 6, 9, 10, 11,
            14, 13, 12,
            15
        };
        unsigned k;
        memcpy(src, c_src, BUFSIZE*sizeof(char));
        bams_byte_reorder_in_place(src, 3, 2, 3);
        for( k=0 ; k<BUFSIZE ; ++k ) {
            CK( src[k] == expected[k] );
        }
    }
    {
        /* 4-byte reverse, stride 1 */
        char src[BUFSIZE];
        char expected[BUFSIZE] = {
            3, 2, 1, 0, 7, 6, 5, 4,
            11, 10, 9, 8, 15, 14, 13, 12
        };
        unsigned k;
        memcpy(src, c_src, BUFSIZE*sizeof(char));
        bams_byte_reorder_in_place(src, 4, 1, BUFSIZE/4);
        for( k=0 ; k<BUFSIZE ; ++k ) {
            CK( src[k] == expected[k] );
        }
    }
    {
        /* 4-byte reverse, stride 2 */
        char src[BUFSIZE];
        char expected[BUFSIZE] = {
            3, 2, 1, 0, 4, 5, 6, 7,
            11, 10, 9, 8, 12, 13, 14, 15
        };
        unsigned k;
        memcpy(src, c_src, BUFSIZE*sizeof(char));
        bams_byte_reorder_in_place(src, 4, 2, BUFSIZE/8);
        for( k=0 ; k<BUFSIZE ; ++k ) {
            CK( src[k] == expected[k] );
        }
    }
}

TEST_CASE( 020_bams_convert_int_to_uint )
{
    const unsigned BUFSIZE = 8;
    {
        int8_t src[BUFSIZE] = {
            0, 127, -128, -1,
            1, 60, -60, 0
        };
        uint8_t expected[BUFSIZE] = {
            128, 255, 0, 0x7F,
            129, 188, 68, 128
        };
        uint8_t *ptr;
        int size, stride;
        unsigned count;
        int k;
        size = sizeof(int8_t);
        stride = 1;
        count = BUFSIZE / stride / size;
        bams_convert_int_to_uint(src, size, stride, count);
        ptr = reinterpret_cast<uint8_t*>(src);
        for( k=0 ; k<BUFSIZE ; ++k ) {
            CK(ptr[k] == expected[k]);
        }
    }
    {
        int16_t src[BUFSIZE] = {
            0, 32767, -32768, -1,
            1, 15361, -15361, 0
        };
        uint16_t expected[BUFSIZE] = {
            32768, 65535, 0, 32767,
            32769, 48129, 17407, 32768
        };
        uint16_t *ptr;
        int size, stride;
        unsigned count;
        int k;
        size = sizeof(int16_t);
        stride = 1;
        count = BUFSIZE / stride;
        bams_convert_int_to_uint(src, size, stride, count);
        ptr = reinterpret_cast<uint16_t*>(src);
        for( k=0 ; k<BUFSIZE ; ++k ) {
            CK(ptr[k] == expected[k]);
        }
    }
    {
        /* 24-bit tests skipped */
    }
    {
        int32_t src[BUFSIZE] = {
            0L, 0x7FFFFFFFL, 0x80000000L, 0xFFFFFFFFL,
            1L, 0x3C013C01L, 0xC3FEC3FFL, 0L
        };
        uint32_t expected[BUFSIZE] = {
            0x80000000L, 0xFFFFFFFFL, 0L, 0x7FFFFFFFL,
            0x80000001L, 0xBC013C01L, 0x43FEC3FFL, 0x80000000L
        };
        uint32_t *ptr;
        int size, stride;
        unsigned count;
        int k;
        size = sizeof(int32_t);
        stride = 1;
        count = BUFSIZE / stride;
        bams_convert_int_to_uint(src, size, stride, count);
        ptr = reinterpret_cast<uint32_t*>(src);
        for( k=0 ; k<BUFSIZE ; ++k ) {
            CK(ptr[k] == expected[k]);
        }
    }
}

TEST_CASE( 020_bams_convert_uint_to_int )
{
    const unsigned BUFSIZE = 8;
    {
        uint8_t src[BUFSIZE] = {
            128, 255, 0, 0x7F,
            129, 188, 68, 128
        };
        int8_t expected[BUFSIZE] = {
            0, 127, -128, -1,
            1, 60, -60, 0
        };
        int8_t *ptr;
        int size, stride;
        unsigned count;
        int k;
        size = sizeof(int8_t);
        stride = 1;
        count = BUFSIZE / stride / size;
        bams_convert_uint_to_int(src, size, stride, count);
        ptr = reinterpret_cast<int8_t*>(src);
        for( k=0 ; k<BUFSIZE ; ++k ) {
            CK(ptr[k] == expected[k]);
        }
    }
    {
        uint16_t src[BUFSIZE] = {
            32768, 65535, 0, 32767,
            32769, 48129, 17407, 32768
        };
        int16_t expected[BUFSIZE] = {
            0, 32767, -32768, -1,
            1, 15361, -15361, 0
        };
        int16_t *ptr;
        int size, stride;
        unsigned count;
        int k;
        size = sizeof(int16_t);
        stride = 1;
        count = BUFSIZE / stride;
        bams_convert_uint_to_int(src, size, stride, count);
        ptr = reinterpret_cast<int16_t*>(src);
        for( k=0 ; k<BUFSIZE ; ++k ) {
            CK(ptr[k] == expected[k]);
        }
    }
    {
        /* 24-bit tests skipped */
    }
    {
        uint32_t src[BUFSIZE] = {
            0x80000000L, 0xFFFFFFFFL, 0L, 0x7FFFFFFFL,
            0x80000001L, 0xBC013C01L, 0x43FEC3FFL, 0x80000000L
        };
        int32_t expected[BUFSIZE] = {
            0L, 0x7FFFFFFFL, 0x80000000L, 0xFFFFFFFFL,
            1L, 0x3C013C01L, 0xC3FEC3FFL, 0L
        };
        int32_t *ptr;
        int size, stride;
        unsigned count;
        int k;
        size = sizeof(int32_t);
        stride = 1;
        count = BUFSIZE / stride;
        bams_convert_uint_to_int(src, size, stride, count);
        ptr = reinterpret_cast<int32_t*>(src);
        for( k=0 ; k<BUFSIZE ; ++k ) {
            CK(ptr[k] == expected[k]);
        }
    }
}

/*********************************************************
 * ALL OF THE TESTS BELOW ASSUME THAT THE BYTE ORDER
 * AND INT/UNIT CONVERSION ROUTINES ABOVE ARE RELIABLE.
 *********************************************************
 */

TEST_CASE( 020_bams_copy_s16le_floatle )
{
    const unsigned BUFSIZE = 8;
    BufferConvertTest<bams_sample_s16le_t, bams_sample_floatle_t> test(BUFSIZE);

    test.copy_func = bams_copy_s16le_floatle;

    test.src[0] = 0.0f;
    test.src[1] = 1.0f;
    test.src[2] = -1.0f;
    test.src[3] = 0.5f;
    test.src[4] = -0.5f;
    test.src[5] = 0.0f;
    test.src[6] = 0.0f;
    test.src[7] = 0.0f;

    test.expected_dest[0] = 0x0000;
    test.expected_dest[1] = 0x7FFF;
    test.expected_dest[2] = 0x8001;
    test.expected_dest[3] = 0x4000;
    test.expected_dest[4] = 0xC000;
    test.expected_dest[5] = 0x0000;
    test.expected_dest[6] = 0x0000;
    test.expected_dest[7] = 0x0000;

#if __BYTE_ORDER == __BIG_ENDIAN
    bams_byte_reorder_in_place(test.src, sizeof(bams_sample_floatle_t), 1, BUFSIZE);
    bams_byte_reorder_in_place(test.expected_dest, sizeof(bams_sample_s16le_t), 1, BUFSIZE);
#else
#endif

    test.run_test();
}

TEST_CASE( 020_bams_copy_s16be_floatle )
{
    const unsigned BUFSIZE = 8;
    BufferConvertTest<bams_sample_s16be_t, bams_sample_floatle_t> test(BUFSIZE);

    test.copy_func = bams_copy_s16be_floatle;

    test.src[0] = 0.0f;
    test.src[1] = 1.0f;
    test.src[2] = -1.0f;
    test.src[3] = 0.5f;
    test.src[4] = -0.5f;
    test.src[5] = 0.0f;
    test.src[6] = 0.0f;
    test.src[7] = 0.0f;

    test.expected_dest[0] = 0x0000;
    test.expected_dest[1] = 0x7FFF;
    test.expected_dest[2] = 0x8001;
    test.expected_dest[3] = 0x4000;
    test.expected_dest[4] = 0xC000;
    test.expected_dest[5] = 0x0000;
    test.expected_dest[6] = 0x0000;
    test.expected_dest[7] = 0x0000;

#if __BYTE_ORDER == __BIG_ENDIAN
    bams_byte_reorder_in_place(test.src, sizeof(bams_sample_floatle_t), 1, BUFSIZE);
#else
    bams_byte_reorder_in_place(test.expected_dest, sizeof(bams_sample_s16be_t), 1, BUFSIZE);
#endif

    test.run_test();
}


TEST_CASE( 020_bams_copy_s16le_floatbe )
{
    const unsigned BUFSIZE = 8;
    BufferConvertTest<bams_sample_s16le_t, bams_sample_floatbe_t> test(BUFSIZE);

    test.copy_func = bams_copy_s16le_floatbe;

    test.src[0] = 0.0f;
    test.src[1] = 1.0f;
    test.src[2] = -1.0f;
    test.src[3] = 0.5f;
    test.src[4] = -0.5f;
    test.src[5] = 0.0f;
    test.src[6] = 0.0f;
    test.src[7] = 0.0f;

    test.expected_dest[0] = 0x0000;
    test.expected_dest[1] = 0x7FFF;
    test.expected_dest[2] = 0x8001;
    test.expected_dest[3] = 0x4000;
    test.expected_dest[4] = 0xC000;
    test.expected_dest[5] = 0x0000;
    test.expected_dest[6] = 0x0000;
    test.expected_dest[7] = 0x0000;

#if __BYTE_ORDER == __BIG_ENDIAN
    bams_byte_reorder_in_place(test.expected_dest, sizeof(bams_sample_s16le_t), 1, BUFSIZE);
#else
    bams_byte_reorder_in_place(test.src, sizeof(bams_sample_floatbe_t), 1, BUFSIZE);
#endif

    test.run_test();
}

TEST_CASE( 020_bams_copy_s16be_floatbe )
{
    const unsigned BUFSIZE = 8;
    BufferConvertTest<bams_sample_s16be_t, bams_sample_floatbe_t> test(BUFSIZE);

    test.copy_func = bams_copy_s16be_floatbe;

    test.src[0] = 0.0f;
    test.src[1] = 1.0f;
    test.src[2] = -1.0f;
    test.src[3] = 0.5f;
    test.src[4] = -0.5f;
    test.src[5] = 0.0f;
    test.src[6] = 0.0f;
    test.src[7] = 0.0f;

    test.expected_dest[0] = 0x0000;
    test.expected_dest[1] = 0x7FFF;
    test.expected_dest[2] = 0x8001;
    test.expected_dest[3] = 0x4000;
    test.expected_dest[4] = 0xC000;
    test.expected_dest[5] = 0x0000;
    test.expected_dest[6] = 0x0000;
    test.expected_dest[7] = 0x0000;

#if __BYTE_ORDER == __BIG_ENDIAN
#else
    bams_byte_reorder_in_place(test.expected_dest, sizeof(bams_sample_s16be_t), 1, BUFSIZE);
    bams_byte_reorder_in_place(test.src, sizeof(bams_sample_floatbe_t), 1, BUFSIZE);
#endif

    test.run_test();
}

TEST_CASE( 020_bams_copy_u16le_floatle )
{
    const unsigned BUFSIZE = 8;
    BufferConvertTest<bams_sample_u16le_t, bams_sample_floatle_t> test(BUFSIZE);

    test.copy_func = bams_copy_u16le_floatle;

    test.src[0] = 0.0f;
    test.src[1] = 1.0f;
    test.src[2] = -1.0f;
    test.src[3] = 0.5f;
    test.src[4] = -0.5f;
    test.src[5] = 0.0f;
    test.src[6] = 0.0f;
    test.src[7] = 0.0f;

    test.expected_dest[0] = 0x8000;
    test.expected_dest[1] = 0xFFFF;
    test.expected_dest[2] = 0x0001;
    test.expected_dest[3] = 0xC000;
    test.expected_dest[4] = 0x4000;
    test.expected_dest[5] = 0x8000;
    test.expected_dest[6] = 0x8000;
    test.expected_dest[7] = 0x8000;

#if __BYTE_ORDER == __BIG_ENDIAN
    bams_byte_reorder_in_place(test.src, sizeof(bams_sample_floatle_t), 1, BUFSIZE);
    bams_byte_reorder_in_place(test.expected_dest, sizeof(bams_sample_u16le_t), 1, BUFSIZE);
#else
#endif

    test.run_test();
}

TEST_CASE( 020_bams_copy_u16be_floatle )
{
    const unsigned BUFSIZE = 8;
    BufferConvertTest<bams_sample_u16be_t, bams_sample_floatle_t> test(BUFSIZE);

    test.copy_func = bams_copy_u16be_floatle;

    test.src[0] = 0.0f;
    test.src[1] = 1.0f;
    test.src[2] = -1.0f;
    test.src[3] = 0.5f;
    test.src[4] = -0.5f;
    test.src[5] = 0.0f;
    test.src[6] = 0.0f;
    test.src[7] = 0.0f;

    test.expected_dest[0] = 0x8000;
    test.expected_dest[1] = 0xFFFF;
    test.expected_dest[2] = 0x0001;
    test.expected_dest[3] = 0xC000;
    test.expected_dest[4] = 0x4000;
    test.expected_dest[5] = 0x8000;
    test.expected_dest[6] = 0x8000;
    test.expected_dest[7] = 0x8000;

#if __BYTE_ORDER == __BIG_ENDIAN
    bams_byte_reorder_in_place(test.src, sizeof(bams_sample_floatle_t), 1, BUFSIZE);
#else
    bams_byte_reorder_in_place(test.expected_dest, sizeof(bams_sample_u16be_t), 1, BUFSIZE);
#endif

    test.run_test();
}

TEST_CASE( 020_bams_copy_u16le_floatbe )
{
    const unsigned BUFSIZE = 8;
    BufferConvertTest<bams_sample_u16le_t, bams_sample_floatbe_t> test(BUFSIZE);

    test.copy_func = bams_copy_u16le_floatbe;

    test.src[0] = 0.0f;
    test.src[1] = 1.0f;
    test.src[2] = -1.0f;
    test.src[3] = 0.5f;
    test.src[4] = -0.5f;
    test.src[5] = 0.0f;
    test.src[6] = 0.0f;
    test.src[7] = 0.0f;

    test.expected_dest[0] = 0x8000;
    test.expected_dest[1] = 0xFFFF;
    test.expected_dest[2] = 0x0001;
    test.expected_dest[3] = 0xC000;
    test.expected_dest[4] = 0x4000;
    test.expected_dest[5] = 0x8000;
    test.expected_dest[6] = 0x8000;
    test.expected_dest[7] = 0x8000;

#if __BYTE_ORDER == __BIG_ENDIAN
    bams_byte_reorder_in_place(test.expected_dest, sizeof(bams_sample_u16le_t), 1, BUFSIZE);
#else
    bams_byte_reorder_in_place(test.src, sizeof(bams_sample_floatbe_t), 1, BUFSIZE);
#endif

    test.run_test();
}

TEST_CASE( 020_bams_copy_u16be_floatbe )
{
    const unsigned BUFSIZE = 8;
    BufferConvertTest<bams_sample_u16be_t, bams_sample_floatbe_t> test(BUFSIZE);

    test.copy_func = bams_copy_u16be_floatbe;

    test.src[0] = 0.0f;
    test.src[1] = 1.0f;
    test.src[2] = -1.0f;
    test.src[3] = 0.5f;
    test.src[4] = -0.5f;
    test.src[5] = 0.0f;
    test.src[6] = 0.0f;
    test.src[7] = 0.0f;

    test.expected_dest[0] = 0x8000;
    test.expected_dest[1] = 0xFFFF;
    test.expected_dest[2] = 0x0001;
    test.expected_dest[3] = 0xC000;
    test.expected_dest[4] = 0x4000;
    test.expected_dest[5] = 0x8000;
    test.expected_dest[6] = 0x8000;
    test.expected_dest[7] = 0x8000;

#if __BYTE_ORDER == __BIG_ENDIAN
#else
    bams_byte_reorder_in_place(test.expected_dest, sizeof(bams_sample_u16be_t), 1, BUFSIZE);
    bams_byte_reorder_in_place(test.src, sizeof(bams_sample_floatbe_t), 1, BUFSIZE);
#endif

    test.run_test();
}

TEST_END()
