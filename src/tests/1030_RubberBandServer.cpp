/*
 * Copyright(c) 2011 by Gabriel M. Beddingfield <gabriel@teuton.org>
 *
 * This file is part of StretchPlayer
 *
 * Tritium is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Tritium is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/**
 * 1030_RubberBandServer.cpp
 *
 * This tests the RubberBandServer object
 */

#include "RubberBandServer.hpp"

#include <vector>
#include <cmath>
#include <cstdio>

#include <iostream>
using namespace std;

// CHANGE THIS TO MATCH YOUR FILE:
#define THIS_NAMESPACE t_RubberBandServer
#include "test_macros.hpp"
#include "test_config.hpp"

using namespace StretchPlayer;

namespace THIS_NAMESPACE
{

    struct Fixture
    {
	// SETUP AND TEARDOWN OBJECTS FOR YOUR TESTS.

	Fixture() {}
	~Fixture() {}
    };

} // namespace THIS_NAMESPACE

TEST_BEGIN( Fixture );

TEST_CASE( 010_defaults )
{
    RubberBandServer rbs(48000);

    rbs.start();
    CK( 0 == rbs.available_read() );
    CK( rbs.available_write() );
    CK( rbs.is_running() );
    sleep(1);
    CK( rbs.is_running() );
    rbs.shutdown();
    rbs.wait();
    CK( ! rbs.is_running() );
}

TEST_CASE( 020_time_stretch_no_stretching_though )
{
    double src_freq, dest_freq;
    double sample_rate;
    std::vector<float> left, right;
    std::vector<float> out_L, out_R;
    const size_t NBUF = 4*2048;
    const size_t NCHUNK = 64;
    double time_ratio = 1.0;
    size_t out_pos, in_pos, k;

    src_freq = 440.0;
    sample_rate = 44100;

    left.insert(left.end(), NBUF, 0);
    right.insert(right.end(), NBUF, 0);

    RubberBandServer rbs(sample_rate);
    rbs.set_segment_size( 64 );
    rbs.start();
    rbs.reset();
    rbs.time_ratio( time_ratio );
    rbs.pitch_scale( 1.0 );

    CK( rbs.is_running() );

    out_L.insert(out_L.end(), size_t( NBUF * time_ratio + 1.0), 0);
    out_R.insert(out_R.end(), size_t( NBUF * time_ratio + 1.0), 0);
    out_pos = 0;
    in_pos = 0;

    // Fill both buffers with sine waves @ 440.0 Hz
    for( k = 0 ; k < NBUF ; ++k ) {
        left[k] = ::sin( 2.0 * M_PI * src_freq * double(k) / sample_rate );
        right[k] = left[k];
        if( ((k % 64) == 0) && k ) {
            while( rbs.available_write() < 64 )
                usleep( 100 );
            in_pos += rbs.write_audio( &left[in_pos], &right[in_pos], 64 );
            out_pos += rbs.read_audio(&out_L[out_pos], &out_R[out_pos], (out_L.size() - out_pos));
        }
    }
    assert( k == NBUF );
    in_pos += rbs.write_audio( &left[in_pos], &right[in_pos], 64 );
    assert( in_pos == k );
    usleep( 1000 );

    size_t LATENCY = rbs.latency();
    if( LATENCY )
        --LATENCY;
    cout << LATENCY << endl;
    const size_t NANALZ = NBUF / 4;
    while( (rbs.available_read() > 0) || (out_pos < (NANALZ + LATENCY)) ) {
            out_pos += rbs.read_audio(&out_L[out_pos], &out_R[out_pos], (out_L.size() - out_pos));
            usleep( 1000 );
    }

    double err = 0.0, x;
    assert( NANALZ < out_L.size() );
    assert( NANALZ + LATENCY <= out_pos );
    for( k = 0 ; k < NANALZ ; ++k ) {
        CK( (out_L[k+LATENCY] - out_R[k+LATENCY]) < 1.0e-6 );
        x = ::sin( 2.0 * M_PI * src_freq * double(k) / sample_rate );
        x = out_L[k+LATENCY] - x;
        x *= x;
        err += x;
    }
    err /= double(NANALZ);
    cout << err << endl;

    FILE *fp;
    fp = fopen("tmp-wave-data-1.txt", "wt");
    for( k = 0 ; k < NANALZ ; ++k ) {
        fprintf(fp, "%zu\t%g\n", k, left[k]);
    }
    fclose(fp);
    fp = fopen("tmp-wave-data-2.txt", "wt");
    for( k = LATENCY ; k < LATENCY + NANALZ ; ++k ) {
        fprintf(fp, "%zu\t%g\n", k-LATENCY, out_L[k]);
    }
    fclose(fp);


    CK( err < 0.001 );

    rbs.shutdown();
    rbs.wait();
}

TEST_CASE( 030_time_stretch )
{
    double src_freq, dest_freq;
    double sample_rate;
    std::vector<float> left, right;
    std::vector<float> out_L, out_R;
    const size_t NBUF = 4*2048;
    const size_t NCHUNK = 64;
    double time_ratio = 0.813;
    size_t out_pos, in_pos, k;

    src_freq = 440.0;
    sample_rate = 44100;

    left.insert(left.end(), NBUF, 0);
    right.insert(right.end(), NBUF, 0);

    RubberBandServer rbs(sample_rate);
    rbs.set_segment_size( 64 );
    rbs.start();
    rbs.reset();
    rbs.time_ratio( time_ratio );
    rbs.pitch_scale( 1.0 );

    CK( rbs.is_running() );

    out_L.insert(out_L.end(), size_t( NBUF * time_ratio + 1.0), 0);
    out_R.insert(out_R.end(), size_t( NBUF * time_ratio + 1.0), 0);
    out_pos = 0;
    in_pos = 0;

    // Fill both buffers with sine waves @ 440.0 Hz
    for( k = 0 ; k < NBUF ; ++k ) {
        left[k] = ::sin( 2.0 * M_PI * src_freq * double(k) / sample_rate );
        right[k] = left[k];
        if( ((k % 64) == 0) && k ) {
            while( rbs.available_write() < 64 )
                usleep( 100 );
            in_pos += rbs.write_audio( &left[in_pos], &right[in_pos], 64 );
            out_pos += rbs.read_audio(&out_L[out_pos], &out_R[out_pos], (out_L.size() - out_pos));
        }
    }
    assert( k == NBUF );
    in_pos += rbs.write_audio( &left[in_pos], &right[in_pos], 64 );
    assert( in_pos == k );
    usleep( 1000 );

    size_t LATENCY = rbs.latency();
    if( LATENCY )
        --LATENCY;
    cout << LATENCY << endl;
    const size_t NANALZ = NBUF / 4;
    while( (rbs.available_read() > 0) || (out_pos < (NANALZ + LATENCY)) ) {
            out_pos += rbs.read_audio(&out_L[out_pos], &out_R[out_pos], (out_L.size() - out_pos));
            usleep( 1000 );
    }

    double err = 0.0, x;
    assert( NANALZ < out_L.size() );
    assert( NANALZ + LATENCY <= out_pos );
    for( k = 0 ; k < NANALZ ; ++k ) {
        CK( (out_L[k+LATENCY] - out_R[k+LATENCY]) < 1.0e-6 );
        x = ::sin( 2.0 * M_PI * src_freq * double(k) / sample_rate );
        x = out_L[k+LATENCY] - x;
        x *= x;
        err += x;
    }
    err /= double(NANALZ);
    cout << err << endl;

    FILE *fp;
    fp = fopen("tmp-030-wave-data-1.txt", "wt");
    for( k = 0 ; k < NANALZ ; ++k ) {
        fprintf(fp, "%zu\t%g\n", k, left[k]);
    }
    fclose(fp);
    fp = fopen("tmp-030-wave-data-2.txt", "wt");
    for( k = LATENCY ; k < LATENCY + NANALZ ; ++k ) {
        fprintf(fp, "%zu\t%g\n", k-LATENCY, out_L[k]);
    }
    fclose(fp);


    CK( err < 0.005 );

    rbs.shutdown();
    rbs.wait();
}

TEST_CASE( 040_pitch_shift )
{
    double src_freq, dest_freq;
    double sample_rate;
    std::vector<float> left, right;
    std::vector<float> out_L, out_R;
    const size_t NBUF = 4*2048;
    const size_t NCHUNK = 64;
    double time_ratio = 1.0;
    double pitch_ratio = ::pow( 2.0, 1.0/12.0 );
    size_t out_pos, in_pos, k;

    src_freq = 440.0;
    sample_rate = 44100;
    dest_freq = src_freq * pitch_ratio;
    cout << "dest_freq == " << dest_freq << endl;

    left.insert(left.end(), NBUF, 0);
    right.insert(right.end(), NBUF, 0);

    RubberBandServer rbs(sample_rate);
    rbs.set_segment_size( 64 );
    rbs.start();
    rbs.reset();
    rbs.time_ratio( time_ratio );
    rbs.pitch_scale( pitch_ratio );

    CK( rbs.is_running() );

    out_L.insert(out_L.end(), size_t( NBUF * time_ratio + 1.0), 0);
    out_R.insert(out_R.end(), size_t( NBUF * time_ratio + 1.0), 0);
    out_pos = 0;
    in_pos = 0;

    // Fill both buffers with sine waves @ 440.0 Hz
    for( k = 0 ; k < NBUF ; ++k ) {
        left[k] = ::sin( 2.0 * M_PI * src_freq * double(k) / sample_rate );
        right[k] = left[k];
        if( ((k % 64) == 0) && k ) {
            while( rbs.available_write() < 64 )
                usleep( 100 );
            in_pos += rbs.write_audio( &left[in_pos], &right[in_pos], 64 );
            out_pos += rbs.read_audio(&out_L[out_pos], &out_R[out_pos], (out_L.size() - out_pos));
        }
    }
    assert( k == NBUF );
    in_pos += rbs.write_audio( &left[in_pos], &right[in_pos], 64 );
    assert( in_pos == k );
    usleep( 1000 );

    size_t LATENCY = rbs.latency();
    if( LATENCY )
        --LATENCY;
    cout << LATENCY << endl;
    const size_t NANALZ = NBUF / 4;
    while( (rbs.available_read() > 0) || (out_pos < (NANALZ + LATENCY)) ) {
            out_pos += rbs.read_audio(&out_L[out_pos], &out_R[out_pos], (out_L.size() - out_pos));
            usleep( 1000 );
    }

    double err = 0.0, x;
    double phase = -0.66391 + M_PI; // The phase is experimentally determined... and could change.
    assert( NANALZ < out_L.size() );
    assert( NANALZ + LATENCY <= out_pos );
    for( k = 0 ; k < NANALZ ; ++k ) {
        CK( (out_L[k+LATENCY] - out_R[k+LATENCY]) < 1.0e-6 );
        x = ::sin( 2.0 * M_PI * dest_freq * double(k) / sample_rate + phase );
        x = out_L[k+LATENCY] - x;
        x *= x;
        err += x;
    }
    err /= double(NANALZ);
    cout << err << endl;

    // Rewrite the input buffer to look like what we expected.
    for( k = 0 ; k < NANALZ ; ++k ) {
        left[k] = ::sin( 2.0 * M_PI * dest_freq * double(k) / sample_rate + phase );
        right[k] = left[k];
    }

    FILE *fp;
    fp = fopen("tmp-040-wave-data-1.txt", "wt");
    for( k = 0 ; k < NANALZ ; ++k ) {
        fprintf(fp, "%zu\t%g\n", k, left[k]);
    }
    fclose(fp);
    fp = fopen("tmp-040-wave-data-2.txt", "wt");
    for( k = LATENCY ; k < LATENCY + NANALZ ; ++k ) {
        fprintf(fp, "%zu\t%g\n", k-LATENCY, out_L[k]);
    }
    fclose(fp);


    CK( err < 0.05 );

    rbs.shutdown();
    rbs.wait();
}

TEST_END()
