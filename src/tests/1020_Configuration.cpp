/*
 * Copyright(c) 2009 by Gabriel M. Beddingfield <gabriel@teuton.org>
 *
 * This file is part of Tritium
 *
 * Tritium is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Tritium is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/**
 * 1020_Configuration.cpp
 *
 * Tests for the StretchPlayer::Configuration class.
 */

#include "config.h"
#include "Configuration.hpp"

// CHANGE THIS TO MATCH YOUR FILE:
#define THIS_NAMESPACE t_Configuration
#include "test_macros.hpp"
#include "test_config.hpp"

using namespace StretchPlayer;

namespace THIS_NAMESPACE
{

    struct Fixture
    {
	// SETUP AND TEARDOWN OBJECTS FOR YOUR TESTS.

	Fixture() {}
	~Fixture() {}
    };

} // namespace THIS_NAMESPACE

TEST_BEGIN( Fixture );

TEST_CASE( 010_defaults )
{
    {
        Configuration c(0, 0);
        CK( c.version() == QString(STRETCHPLAYER_VERSION) );
        CK( c.ok() );
        /* c.driver default not regression tested */
        /* c.audio_device default not regression tested */
        CK( c.sample_rate() == 44100 );
        CK( c.period_size() == 1024 );
        CK( c.periods_per_buffer() == 2 );
        CK( c.startup_file() == QString() );
        CK( c.autoconnect() == true );
        /* c.compositing default not regression tested */
        CK( c.quiet() == false );
        CK( c.help() == false );
    }
}

TEST_CASE( 020_misc_options )
{
    {
        int argc = 2;
        const char* argv[] = { "test", "-q" };
        Configuration c(argc, const_cast<char**>(argv));
        CK( c.quiet() == true );
    }
    {
        int argc = 2;
        const char* argv[] = { "test", "--quiet" };
        Configuration c(argc, const_cast<char**>(argv));
        CK( c.quiet() == true );
    }
    {
        int argc = 2;
        const char* argv[] = { "test", "-J" };
        Configuration c(argc, const_cast<char**>(argv));
        CK( c.driver() == Configuration::JackDriver );
        CK( c.driver() != Configuration::AlsaDriver );
    }
    {
        int argc = 2;
        const char* argv[] = { "test", "--jack" };
        Configuration c(argc, const_cast<char**>(argv));
        CK( c.driver() == Configuration::JackDriver );
        CK( c.driver() != Configuration::AlsaDriver );
    }
    {
        int argc = 2;
        const char* argv[] = { "test", "-A" };
        Configuration c(argc, const_cast<char**>(argv));
        CK( c.driver() == Configuration::AlsaDriver );
        CK( c.driver() != Configuration::JackDriver );
    }
    {
        int argc = 2;
        const char* argv[] = { "test", "--alsa" };
        Configuration c(argc, const_cast<char**>(argv));
        CK( c.driver() == Configuration::AlsaDriver );
        CK( c.driver() != Configuration::JackDriver );
    }
    {
        int argc = 3;
        const char* argv[] = { "test", "-d", "hw:6" };
        Configuration c(argc, const_cast<char**>(argv));
        CK( c.audio_device() == QString("hw:6") );
    }
    {
        int argc = 3;
        const char* argv[] = { "test", "--device", "hw:6" };
        Configuration c(argc, const_cast<char**>(argv));
        CK( c.audio_device() == QString("hw:6") );
    }
    {
        int argc = 3;
        const char* argv[] = { "test", "-r", "96000" };
        Configuration c(argc, const_cast<char**>(argv));
        CK( c.sample_rate() == 96000 );
    }
    {
        int argc = 3;
        const char* argv[] = { "test", "--sample-rate", "96000" };
        Configuration c(argc, const_cast<char**>(argv));
        CK( c.sample_rate() == 96000 );
    }
    {
        int argc = 2;
        const char* argv[] = { "test", "foo.wav" };
        Configuration c(argc, const_cast<char**>(argv));
        CK( c.startup_file() == QString("foo.wav") );
    }

}

TEST_CASE( 030_some_combinations )
{
    {
        int argc = 4;
        const char* argv[] = { "test", "-q", "-J", "--alsa" };
        Configuration c(argc, const_cast<char**>(argv));
        CK( c.quiet() == true );
        CK( c.driver() == Configuration::AlsaDriver );
    }
    {
        int argc = 6;
        const char* argv[] = { "test", "--alsa", "-d", "hw:6", "--sample-rate", "44100" };
        Configuration c(argc, const_cast<char**>(argv));
        CK( c.driver() == Configuration::AlsaDriver );
        CK( c.audio_device() == QString("hw:6") );
        CK( c.sample_rate() == 44100 );
    }
    {
        int argc = 4;
        const char* argv[] = { "test", "--jack", "-x", "bar.ogg" };
        Configuration c(argc, const_cast<char**>(argv));
        CK( c.driver() == Configuration::JackDriver );
        CK( c.startup_file() == QString("bar.ogg") );
        CK( c.ok() );
        CK( c.autoconnect() == false );
    }
}

TEST_END()
